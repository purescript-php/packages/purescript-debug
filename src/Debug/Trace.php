<?php

// // Alias require to prevent webpack or browserify from actually requiring.
// $req = typeof module === "undefined" ? undefined : module.require;
// $util = req === undefined ? undefined : req("util");

$exports['trace'] = function () {
  return function ($x) {
    return function ($k) use (&$x) {
      // node only recurses two levels into an object before printing
      // "[object]" for further objects when using console.log()
      echo var_dump(util);
        // console.log(util.inspect(x, { depth: null, colors: true }));
      return $k([]);
    };
  };
};

$exports['spy'] = function () {
  return function ($tag) {
    return function ($x) use (&$tag) {
      echo $tag . ": " . var_dump($x);
      return $x;
    };
  };
};
